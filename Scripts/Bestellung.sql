create table "Bestellung"
(
    "BenutzerId"          integer not null
        constraint "BenutzerID_Id_Users"
            references "Users",
    id                    integer not null
        constraint bestellung_pk
            primary key,
    "BestellungGoodiesId" integer
        constraint "BestellungGoodiesId_Id_BestellungGoodies"
            references "BestellungGoodies"
);

alter table "Bestellung"
    owner to postgres;

alter table "Bestellung" alter column "BestellungGoodiesId" drop not null;


