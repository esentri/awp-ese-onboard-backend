create table "Attachment"
(
    "Id"    integer not null
        constraint attachment_pk
            primary key,
    "Name"  varchar,
    "Datum" date
);

alter table "Attachment"
    owner to postgres;

create table "Kalender"
(
    "Id"      integer not null
        constraint kalender_pk
            primary key,
    "Comment" varchar(2000),
    "Datum"   date
);

alter table "Kalender"
    owner to postgres;


create table "Goodies"
(
    "Name"      varchar(100) not null,
    "Id"        integer      not null
        constraint goodies_pk
            primary key,
    "Kategorie" varchar(50),
    "Comment"   varchar(2000)
);

alter table "Goodies"
    owner to postgres;

create table "TeamProject"
(
    "Id"         integer not null
        constraint teamproject_pk
            primary key,
    "PersonioId" integer not null,
    "Name"       varchar
);

alter table "TeamProject"
    owner to postgres;

create unique index teamproject_personioid_uindex
    on "TeamProject" ("PersonioId");

create table "Users"
(
    id              integer     not null
        constraint Users_pk
            primary key,
    "Name"          varchar(45) not null,
    "Rolle"         integer     not null,
    "Size"          varchar,
    "Skills"        varchar,
    "teamProjectId" integer     not null
        constraint "TeamProjectId"
            references "TeamProject"
);

alter table "Users"
    owner to postgres;

create unique index userrs_name_uindex
    on "Users" ("Name");

create table "Bestellung"
(
    "BenutzerId"          integer not null
        constraint "BenutzerID_Id_Users"
            references "Users",
    id                    integer not null
        constraint bestellung_pk
            primary key,
    "BestellungGoodiesId" integer

);

alter table "Bestellung"
    owner to postgres;


create table "BestellungGoodies"
(
    "Id"            integer not null
        constraint bestellunggoodies_pk
            primary key,
    "BestellungsId" integer not null
        constraint "BestellungsId_Id_Bestellung"
            references "Bestellung",
    "GoodiesId"     integer not null
        constraint "GoodiesId_Id_Goodies"
            references "Goodies"
);

alter table "BestellungGoodies"
    owner to postgres;

Alter table "Bestellung" add constraint "BestellungGoodiesId_Id_BestellungGoodies" FOREIGN KEY ("BestellungGoodiesId")
    references "BestellungGoodies"
