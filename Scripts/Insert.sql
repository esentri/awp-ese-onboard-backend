--Goddies Tabelle
Insert into goodies
   (comment, kategorie, name)
values (
        'MacBook 15 Zoll',
        'Laptop',
        '15 Zoll'
       );

Insert into goodies
(comment, kategorie, name)
values (
        'MacBook 17 Zoll',
        'Laptop',
        '17 Zoll'
       );

Insert into goodies
    (comment, kategorie, name)
values (
        'Airpods',
         'Kopfhörer',
         'Airpods 3. Generation'
       );

Insert into goodies
    (comment, kategorie, name)
values (
            'Headset',
           'Kopfhörer',
           'Headset Apple'
       );

Insert into goodies
    (comment, kategorie, name)
values (
            'T-Shirt',
           'Kleidung',
           'Weißes esentri Poloshirt'
       );

Insert into goodies
(comment, kategorie, name)
values (
            'Hoodie',
           'Kleidung',
           'Grauer esentri Hoodie'
       );

Insert into goodies
(comment, kategorie, name)
values (
            'Buch Clean Code',
           'Fachbuch',
           'Clean Code'
       );

Insert into goodies
(comment, kategorie, name)
values (
            'Clean Architecture',
           'Fachbuch',
           'Clean Architecture'
       );

insert into goodies
(comment, kategorie, name)
values(
       null,
       'Wunschbox',
       'Wunschbox'
      );

insert into goodies
(comment, kategorie, name)
values(
       'Maus',
       'Maus',
       'Maus'
      );

insert into goodies
(comment, kategorie, name)
values (
        'Tastatur',
        'Tastatur',
        'Tastatur'
       );

insert into goodies
(comment, kategorie, name)
values (
        'Laptopständer',
        'Laptopständer',
        'Laptopständer'
       );


-----------------------------------------------------
--Kreis Tabelle

Insert into kreis
(id, name, personio_id)
values (
        1,
        'Onboarding App',
        1
               );

Insert into kreis
    (id, name, personio_id)
values (
           2,
           'Digitalisierung',2

       );
-----------------------------------------------------
--User Tabelle
--Rolle 0 = ADMIN
--Rolle 1 = Neuer Mitarbeiter

Insert into users
(id, bestellt, name, rolle, size, skills, kreis_id, passwort)
values (
        1,
        0,
        'Robin Gscheidle',
        1,
        'M',
        'Frontend',
        1,
        'user'
       );

Insert into users
(id, bestellt, name, rolle, size, skills, kreis_id)
values (
           2,
            0,
           'Peter Random',
           1,
           'L',
           'Backend',
           1
       );

Insert into users
(id, bestellt, name, rolle, size, skills, kreis_id, passwort)
values (
           3,
            0,
           'Eva Fiserova',
           0,
           '',
           'Backend',
           2,
        'admin'
       );

Insert into users
(id, bestellt, name, rolle, size, skills, kreis_id)
values (
           4,
            0,
           'Anita Willer',
           1,
           'S',
           'Key Account Manager',
           2
       );

Insert into users
(id, bestellt, name, rolle, size, skills, kreis_id)
values (
           5,
            0,
           'Bianca Flura',
           0,
           '',
           'UX Design',
           2
       );

-----------------------------------------------------
-- Kalender Tabelle

Insert into kalender
(id, comment, datum)
values (
            1,
            'Begrüßungsveranstaltung',
            '2022-06-01'
       );

Insert into kalender
(id, comment, datum)
values (
           2,
           'Willkommensgespräch',
           '2022-06-01'
       );

Insert into kalender
(id, comment, datum)
values (
           3,
           'Ausgabe Hardware',
           '2022-06-02'
       );
-----------------------------------------------------
--Bestellung Tabelle
--"BestellungGoodiesId" ist zuerst null

Insert into bestellung
(benutzer_id, bestellt)
values (
           1,
        false
       );

Insert into bestellung
(benutzer_id, bestellt)
values (
           1,
        false
       );

Insert into bestellung
(benutzer_id, bestellt)
values (
           1,
        false
       );

Insert into bestellung
(benutzer_id, bestellt)
values (
           2,
        false
       );

Insert into bestellung
(benutzer_id, bestellt)
values (
           2,
        false
       );

Insert into bestellung
(benutzer_id, bestellt)
values (
           2,
        false
       );

-----------------------------------------------------
-- BestellungGoodies Tabelle

Insert into bestellung_goodies
(goodies_id)
values (
           1
       );

Insert into bestellung_goodies
(goodies_id)
values (
           3
       );

Insert into bestellung_goodies
(goodies_id)
values (
           5
       );

Insert into bestellung_goodies
(goodies_id)
values (
           8
       );

Insert into bestellung_goodies
(goodies_id)
values (
           2
       );

Insert into bestellung_goodies
(goodies_id)
values (
           4
       );

Insert into bestellung_goodies
(goodies_id)
values (
           5
       );
-----------------------------------------------------
--Daten in Tabelle Bestellung updaten, "BestellungGoodiesId" enthält Daten

UPDATE bestellung
    set bestellung_goodies_id = 1
where id = 1;

UPDATE bestellung
set bestellung_goodies_id = 2
where id = 2;

UPDATE bestellung
set bestellung_goodies_id = 3
where id = 3;

UPDATE bestellung
set bestellung_goodies_id = 4
where id = 4;

UPDATE bestellung
set bestellung_goodies_id = 5
where id = 5;

UPDATE bestellung
set bestellung_goodies_id = 6
where id = 6;

UPDATE bestellung
set bestellung_goodies_id = 7
where id = 7;

-----------------------------------------------------
--Attachment Tabelle

Insert into bilder
(id, datum, name)
values (
           1,
           '2022-05-05',
            'esentri Logo'
       );

Insert into bilder
(id, datum, name)
values (
           2,
           '2022-05-05',
           'Profilbild'
       );

-----------------------------------------------------
-- ErsterTag Tabelle

CREATE SEQUENCE ersterTag_sequence
    start 1
    increment 1;

INSERT into erster_tag
(id, dashboard_text)
values (
        nextval('ersterTag_sequence'),
        'Sicherlich bist du schon aufgeregt und kannst es kaum erwarten anzufangen. ' ||
        'Damit am ersten Tag alles glatt läuft, haben wir hier eine Übersicht für Dich zusammen gestellt.'
       );

-----------------------------------------------------
--Wunschbox Tabele

INSERT into wunschbox
(id, benutzer_id)
values (
        1,
        1
       );

INSERT into wunschbox
(id, benutzer_id)
values (
        2,
        2
       );
