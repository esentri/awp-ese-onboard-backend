create table "BestellungGoodies"
(
    "Id"            integer not null
        constraint bestellunggoodies_pk
            primary key,
    "BestellungsId" integer not null
        constraint "BestellungsId_Id_Bestellung"
            references "Bestellung",
    "GoodiesId"     integer not null
        constraint "GoodiesId_Id_Goodies"
            references "Goodies"
);

alter table "BestellungGoodies"
    owner to postgres;




