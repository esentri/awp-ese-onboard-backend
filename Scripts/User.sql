create table "User"
(
    id              integer     not null
        constraint User_pk
            primary key,
    "Name"          varchar(45) not null,
    "Rolle"         integer     not null,
    "Size"  varchar,
    "Skills"        varchar,
    "teamProjectId" integer     not null
        constraint "TeamProjectId"
            references "TeamProject"
);

alter table "User"
    owner to postgres;

create unique index user_name_uindex
    on "User" ("Name");


