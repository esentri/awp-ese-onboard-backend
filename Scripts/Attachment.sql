create table "Attachment"
(
    "Id"    integer not null
        constraint attachment_pk
            primary key,
    "Name"  varchar,
    "Datum" date
);

alter table "Attachment"
    owner to postgres;


