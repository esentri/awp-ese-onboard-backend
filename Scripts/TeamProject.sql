create table "TeamProject"
(
    "Id"         integer not null
        constraint teamproject_pk
            primary key,
    "PersonioId" integer not null,
    "Name"       varchar
);

alter table "TeamProject"
    owner to postgres;

create unique index teamproject_personioid_uindex
    on "TeamProject" ("PersonioId");


