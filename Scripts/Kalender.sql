create table "Kalender"
(
    "Id"      integer not null
        constraint kalender_pk
            primary key,
    "Comment" varchar(2000),
    "Datum"   date
);

alter table "Kalender"
    owner to postgres;

