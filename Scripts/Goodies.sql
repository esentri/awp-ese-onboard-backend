create table "Goodies"
(
    "Name"      varchar(100) not null,
    "Id"        integer      not null
        constraint goodies_pk
            primary key,
    "Kategorie" varchar(50),
    "Comment"   varchar(2000)
);

alter table "Goodies"
    owner to postgres;


