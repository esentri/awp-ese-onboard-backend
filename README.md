# README #

Diese ReadMe beschreibt die Installationsschritte, die notwendig sind, um das Backend fehlerfrei zu starten sowie den Start des Backends.

### Übersicht über das Repository ###

* Beschreibung
  * Backend der esentri Onboarding App
* Version
  * 1.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Wie startet man es? ###

* Installationsschritte
  * JDK (Java), maven und Docker müssen vorhanden sein
  * Installation der PostgreSQL Datenbank:
    * docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -d -p 5432:5432 postgres
  * Einmaliges Ausführen im Backend Terminal:
    * mvn -N io.takari:maven:wrapper
    * ./mvnw spring-boot:run

* Datenbankkonfiguration
  * Anbindung der Datenbank in IntelliJ IDEA:
      * Rechts auf Database klicken
      * Plus-Symbol anklicken
      * Data Source -> PostgreSQL  auswählen
  *Mockdaten in die Datenbank einpflegen:
          * Im Scripts Ordner das Insert.sql Skript ausführen
        
* Backend starten
  * Run BackendApplication
  
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact