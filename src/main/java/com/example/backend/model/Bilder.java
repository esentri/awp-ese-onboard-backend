package com.example.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;
@Entity
@Table(name = "Bilder")
public class Bilder {
    @Id
    private Long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Datum")
    private Date datum;
    @Column(name = "BaseString")
    private String baseString;

    public Bilder(){
    }

    public Bilder(Long id, String name, Date datum, String baseString){
        this.id = id;
        this.name = name;
        this.datum = datum;
        this.baseString = baseString;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getBaseString() {
        return baseString;
    }

    public void setBaseString(String baseString) {
        this.baseString = baseString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bilder that = (Bilder) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, datum);
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", datum=" + datum +
                '}';
    }
}
