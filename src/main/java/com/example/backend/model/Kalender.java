package com.example.backend.model;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Kalender")
public class Kalender {

    @Id
    private Long id;
    @Column(name = "Comment")
    private String comment;
    @Column(name = "Datum")
    private Date datum;

    public Kalender(){}

    public Kalender(Long id, String comment, Date datum){
        this.id = id;
        this.comment = comment;
        this.datum = datum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kalender kalender = (Kalender) o;
        return Objects.equals(id, kalender.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Kalender{" +
                "id=" + id +
                ", comment='" + comment + '\'' +
                ", datum=" + datum +
                '}';
    }
}
