package com.example.backend.model;

import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "Goodies")
public class Goodies {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Kategorie")
    private String kategorie;
    @Column(name = "Comment")
    private String comment;
    @Column(columnDefinition = "TEXT")
    @Lob
    private String baseString;

    public Goodies(){}



    public Goodies(Long id, String name, String kategorie, String comment){
        this.id = id;
        this.name = name;
        this.kategorie = kategorie;
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKategorie() {
        return kategorie;
    }

    public void setKategorie(String kategorie) {
        this.kategorie = kategorie;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getBaseString() {
        return baseString;
    }
    public void setBaseString(String baseString) {
        this.baseString = baseString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Goodies goodies = (Goodies) o;
        return Objects.equals(id, goodies.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Goodies{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", kategorie='" + kategorie + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
