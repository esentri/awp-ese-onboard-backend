package com.example.backend.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Kreis")
public class Kreis {

    @Id
    private Long id;
    @Column(name = "PersonioId")
    private Long personioId;
    @Column(name = "Name")
    private String name;

    public Kreis(){}

    public Kreis(Long id, Long personioId, String name){
        this.id = id;
        this.personioId = personioId;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonioId() {
        return personioId;
    }

    public void setPersonioId(Long personioId) {
        this.personioId = personioId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kreis that = (Kreis) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "TeamProject{" +
                "id=" + id +
                ", personioId=" + personioId +
                ", name='" + name + '\'' +
                '}';
    }
}
