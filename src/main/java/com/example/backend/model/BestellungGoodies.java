package com.example.backend.model;

import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "BestellungGoodies")
public class BestellungGoodies {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "GoodiesId")
    private Long goodiesId;

    public BestellungGoodies(){}

    public BestellungGoodies(Long id, Long goodiesId){
        this.id = id;
        this.goodiesId = goodiesId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodiesId() {
        return goodiesId;
    }

    public void setGoodiesId(Long goodiesId) {
        this.goodiesId = goodiesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BestellungGoodies that = (BestellungGoodies) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "BestellungGoodies{" +
                "id=" + id +
                ", goodiesId=" + goodiesId +
                '}';
    }
}
