package com.example.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "Wunschbox")
public class Wunschbox {

    @Id
    private Long id;

    @Column(name = "BenutzerId")
    private Long benutzerId;

    @Column(name = "Text")
    private String text;

    public Wunschbox(){};

    public Wunschbox(Long id, Long benutzerId, String text){
        this.id = id;
        this.benutzerId = benutzerId;
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBenutzerId() {
        return benutzerId;
    }

    public void setBenutzerId(Long benutzerId) {
        this.benutzerId = benutzerId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wunschbox wunschbox = (Wunschbox) o;
        return Objects.equals(id, wunschbox.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Wunschbox{" +
                "id=" + id +
                ", benutzerId=" + benutzerId +
                ", text='" + text + '\'' +
                '}';
    }
}
