package com.example.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "ErsterTag")
public class ErsterTag {

    @Id
    private Long id;

    @Column(name = "DashboardText")
    private String dashboardText;

    public ErsterTag(){}

    public ErsterTag(Long id, String dashboardText){
        this.id = id;
        this.dashboardText = dashboardText;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDashboardText() {
        return dashboardText;
    }

    public void setDashboardText(String dashboardText) {
        this.dashboardText = dashboardText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ErsterTag ersterTag = (ErsterTag) o;
        return id.equals(ersterTag.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "ErsterTag{" +
                "id=" + id +
                ", dashboardText='" + dashboardText + '\'' +
                '}';
    }
}
