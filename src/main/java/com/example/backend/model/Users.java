package com.example.backend.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Users")
public class Users {
    @Id
    private Long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Rolle")
    private int rolle;
    @Column(name = "Size")
    private String size;
    @Column(name = "Skills")
    private String skills;
    @Column(name = "KreisId")
    private Long kreisId;
    @Column(name = "Bestellt")
    private int bestellt;

    @Column(name = "Passwort")
    private String passwort;


    public Users(){}

    public Users(Long id, String name, int rolle, String size, String skills, Long kreisId, int bestellt, String passwort){
        this.id = id;
        this.name = name;
        this.rolle = rolle;
        this.size = size;
        this.skills = skills;
        this.kreisId = kreisId;
        this.bestellt = bestellt;
        this.passwort = passwort;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRolle() {
        return rolle;
    }

    public void setRolle(int rolle) {
        this.rolle = rolle;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public Long getKreisId() {
        return kreisId;
    }

    public void setKreisId(Long kreisId) {
        this.kreisId = kreisId;
    }

    public int getBestellt() {
        return bestellt;
    }

    public void setBestellt(int bestellt) {
        this.bestellt = bestellt;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Users users = (Users) o;
        return Objects.equals(id, users.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", rolle=" + rolle +
                ", size='" + size + '\'' +
                ", skills='" + skills + '\'' +
                ", kreisId=" + kreisId +
                '}';
    }

}
