package com.example.backend.model;

import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "Bestellung")
public class Bestellung {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "benutzer_id")
    private Long benutzerId;
    @Column(name = "bestellung_goodies_id")
    private Long bestellungGoodiesId;

    @Column(name = "Bestellt")
    private Boolean bestellt;

    public Bestellung(){}

    public Bestellung(Long id, Long benutzerId, Long bestellungGoodiesId, boolean bestellt, String wunschbox){
        this.id = id;
        this.benutzerId = benutzerId;
        this.bestellungGoodiesId = bestellungGoodiesId;
        this.bestellt = bestellt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBenutzerId() {
        return benutzerId;
    }

    public void setBenutzerId(Long benutzerId) {
        this.benutzerId = benutzerId;
    }

    public Long getBestellungGoodiesId() {
        return bestellungGoodiesId;
    }

    public void setBestellungGoodiesId(Long bestellungGoodiesId) {
        this.bestellungGoodiesId = bestellungGoodiesId;
    }

    public boolean isBestellt() {
        return bestellt;
    }

    public void setBestellt(boolean bestellt) {
        this.bestellt = bestellt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bestellung that = (Bestellung) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Bestellung{" +
                "id=" + id +
                ", benutzerId=" + benutzerId +
                ", bestellungGoodiesId=" + bestellungGoodiesId +
                ", bestellt=" + bestellt +
                '}';
    }
}
