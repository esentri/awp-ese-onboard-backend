package com.example.backend.controller;

import com.example.backend.exception.ResourceNotFoundException;
import com.example.backend.model.BestellungGoodies;
import com.example.backend.repository.BestellungGoodiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class BestellungGoodiesController {

    @Autowired
    private BestellungGoodiesRepository bestellungGoodiesRepository;

    // get all bestellungGoodies
    @GetMapping("/bestellungGoodies")
    public List<BestellungGoodies> getAllBestellungGoodies(){
        return bestellungGoodiesRepository.findAll();
    }

    // get bestellungGoodies by id rest api
    @GetMapping("/bestellungGoodies/{id}")
    public ResponseEntity<BestellungGoodies> getBestellungGoodiesById(@PathVariable Long id) {
        BestellungGoodies bestellungGoodies = bestellungGoodiesRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("BestellungGoodies not exist with id :" + id));
        return ResponseEntity.ok(bestellungGoodies);
    }

    @GetMapping("/bestellungGoodies/benutzer/{benutzerId}/{kategorie}")
    public ResponseEntity<Long> getBestellungGoodiesIdByBenutzerUndKategorie(@PathVariable Long benutzerId, @PathVariable String kategorie){
        try{
            List<Long> alleid = bestellungGoodiesRepository.getBestellungGoodiesIdByBenutzer(benutzerId);
            List<Long> id = bestellungGoodiesRepository.getBestellungGoodiesIdByKategorie(kategorie);
            Long result = null;
            for (Long i: alleid
                 ) {
                if(id.contains(i)){ result = i;}
            }
            if (result == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            else
            return ResponseEntity.ok(result);
        } catch (ResourceNotFoundException e){
            throw new ResourceNotFoundException("Bestellung wurde nicht gefunden");
        }
    }

    // create bestellungGoodies rest api
    @PostMapping("/bestellungGoodies")
    public BestellungGoodies createBestellungGoodies(@RequestBody BestellungGoodies bestellungGoodies) {
        return bestellungGoodiesRepository.save(bestellungGoodies);
    }

    @GetMapping("/bestellungGoodies/lastId")
    public Long getLastBestellungGoodiesId(){
        return bestellungGoodiesRepository.getLastInsertedId();
    }

    @PutMapping("/bestellungGoodies/{id}")
    public Optional<BestellungGoodies> updateBestellungGoodies(@RequestBody BestellungGoodies bestellungGoodies, @PathVariable Long id){
        return bestellungGoodiesRepository.findById(id)
                .map(bg1 -> {
                    bg1.setGoodiesId(bestellungGoodies.getGoodiesId());
                    return bestellungGoodiesRepository.save(bg1);
                });


    }
}
