package com.example.backend.controller;

import com.example.backend.exception.ResourceNotFoundException;
import com.example.backend.model.Kalender;
import com.example.backend.repository.KalenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class KalenderController {

    @Autowired
    private KalenderRepository kalenderRepository;

    // get all kalender
    @GetMapping("/kalender")
    public List<Kalender> getAllKalender(){
        return kalenderRepository.findAll();
    }

    // get kalender by id rest api
    @GetMapping("/kalender/{id}")
    public ResponseEntity<Kalender> getKalenderById(@PathVariable Long id) {
        Kalender kalender = kalenderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Kalender not exist with id :" + id));
        return ResponseEntity.ok(kalender);
    }

    // create kalender rest api
    @PostMapping("/kalender")
    public Kalender createKalender(@RequestBody Kalender kalender) {
        return kalenderRepository.save(kalender);
    }
}
