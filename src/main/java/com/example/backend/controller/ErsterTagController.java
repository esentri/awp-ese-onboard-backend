package com.example.backend.controller;

import com.example.backend.exception.ResourceNotFoundException;
import com.example.backend.model.ErsterTag;
import com.example.backend.repository.ErsterTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class ErsterTagController {

    @Autowired
    private ErsterTagRepository ersterTagRepository;

    @GetMapping("/ersterTag")
    public List<ErsterTag> getAllErsteTage(){
        return ersterTagRepository.findAll();
    }

    // get ersterTag by id rest api
    @GetMapping("/ersterTag/{id}")
    public ResponseEntity<ErsterTag> getErsterTagById(@PathVariable Long id) {
        ErsterTag ersterTag = ersterTagRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ErsterTag not exist with id :" + id));
        return ResponseEntity.ok(ersterTag);
    }

    // get ersterTag text by id
    @GetMapping("/ersterTag/text/{id}")
    public String getErsterTagTextById(@PathVariable Long id){
        String text = ersterTagRepository.findErsterTagTextById(id);
        return text;
    }

    // create ersterTag
    @PostMapping("/ersterTag")
    public ErsterTag createErsterTag(@RequestBody ErsterTag ersterTag) {
        return ersterTagRepository.save(ersterTag);
    }

    @PutMapping("/ersterTag/{id}")
    public Optional<ErsterTag> updateErsterTag(@RequestBody ErsterTag ersterTag, @PathVariable Long id){
        return ersterTagRepository.findById(id)
                .map(ersterTag1 -> {
                    ersterTag1.setDashboardText(ersterTag.getDashboardText());
                    return ersterTagRepository.save(ersterTag1);
                });

    }
}
