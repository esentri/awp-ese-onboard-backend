package com.example.backend.controller;

import com.example.backend.exception.BadRequestException;
import com.example.backend.exception.ResourceNotFoundException;
import com.example.backend.model.Users;
import com.example.backend.repository.BenutzerRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class BenutzerController {
    @Autowired
    private BenutzerRepository benutzerRepository;

    // get all users
    @GetMapping("/benutzer")
    public List<Users> getAllUsers() {
        try {
            return benutzerRepository.findAll();
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("Benutzer wurde nicht gefunden");

        } catch (BadRequestException e) {
            throw new BadRequestException("versuchen sie die Seite neu zu Laden");
        }
    }
    // get user by id rest api
    @GetMapping("/benutzer/{id}")
    public ResponseEntity<Users> getUserById(@PathVariable Long id) {
        try{
            Users users = benutzerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + id));
                return ResponseEntity.ok(users);

        } catch (BadRequestException e) {
            throw new BadRequestException("versuchen sie die Seite neu zu Laden");
        }
    }

    @GetMapping("/benutzer/{name}")
    public ResponseEntity<Users> getUserById(@PathVariable String name) {
        try{
            Users users = benutzerRepository.findUserByName(name);
                    //.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + name));
            return ResponseEntity.ok(users);

        } catch (BadRequestException e) {
            throw new BadRequestException("versuchen sie die Seite neu zu Laden");
        }
    }


    @GetMapping("/benutzer/kreis/{id}")
    public List <Users> getUserByKreisId(@PathVariable Long id) {
        try{
        List<Users> findUsers = benutzerRepository.findUserByKreis(id);
        return findUsers;

        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("Kreis wurde nicht gefunden");

        } catch (BadRequestException e) {
            throw new BadRequestException("versuchen sie die Seite neu zu Laden");
        }
    }


    // create user rest api
    @PostMapping("/benutzer")
    public Users createUser(@RequestBody Users users) {
        try {
            return benutzerRepository.save(users);
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("Benutzer wurde nicht gefunden");
        }
    }

    @GetMapping("/benutzer/size/{id}")
    public String getSizeById(@PathVariable Long id){
        Users u = benutzerRepository.findUserById(id);
        String size = u.getSize();
        return size;
    }

    @PutMapping("/benutzer/size/{id}")
    public Users updateSizeUser(@RequestBody Users users, @PathVariable Long id){
        try{
            Users u = benutzerRepository.findUserById(id);
            u.setSize(users.getSize());
            final Users updatedUser = benutzerRepository.save(u);
            return updatedUser;

        } catch (ResourceNotFoundException e){
            throw new ResourceNotFoundException("Benutzer wurde nicht gefunden");
        }
    }

    @GetMapping("/benutzer/bestellt/{id}")
    public int getBestelltById(@PathVariable Long id){
        try{
            int bestellt = benutzerRepository.findBestelltById(id);
            return bestellt;

        } catch (ResourceNotFoundException e){
            throw new ResourceNotFoundException("Benutzer wurde nicht gefunden");
        }
    }

    @PutMapping("/benutzer/bestellt/{id}")
    public void updateBestelltById(@RequestBody Users users, @PathVariable Long id){
        try{
            Users u = benutzerRepository.findUserById(id);
            u.setBestellt(users.getBestellt());
            benutzerRepository.save(u);

        } catch (ResourceNotFoundException e){
            throw new ResourceNotFoundException("Benutzer wurde nicht gefunden");
        }
    }
}
