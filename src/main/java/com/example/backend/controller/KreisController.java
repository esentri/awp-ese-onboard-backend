package com.example.backend.controller;

import com.example.backend.exception.BadRequestException;
import com.example.backend.exception.ResourceNotFoundException;
import com.example.backend.model.Kreis;
import com.example.backend.repository.KreisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class KreisController {
    @Autowired
    private KreisRepository kreisRepository;

    // get all team projects
    @GetMapping("/kreis")
    public List<Kreis> getAllKreis(){
        return kreisRepository.findAll();
    }

    // get team project by id rest api
    @GetMapping("/kreis/{id}")
    public ResponseEntity<Kreis> getTeamProjectById(@PathVariable Long id) {
        try{
            Kreis kreis = kreisRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException("Teamprojekt existiert nicht mit id :" + id));
                return ResponseEntity.ok(kreis);
        } catch (MethodArgumentTypeMismatchException e){
            throw new BadRequestException("Eingabe war fehlerhaft");
        }
    }

    // create team project rest api
    @PostMapping("/kreis")
    public Kreis createTeamProject(@RequestBody Kreis kreis) {
        try {
            return kreisRepository.save(kreis);
        } catch (ResourceNotFoundException e){
            throw new ResourceNotFoundException("Kreis wurde nicht gefunden");
        }
    }
}
