package com.example.backend.controller;

import java.util.List;
import java.util.Optional;

import com.example.backend.exception.BadRequestException;
import com.example.backend.exception.ResourceNotFoundException;
import com.example.backend.model.Bilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.backend.model.Goodies;
import com.example.backend.repository.GoodiesRepository;
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class GoodiesController {
    @Autowired
    private GoodiesRepository goodiesRepository;

    // get all goodies
    @GetMapping("/goodies")
    public List<Goodies> getAllGoodies(){
        return goodiesRepository.findAll();
    }

    // get goodie by id rest api
    @GetMapping("/goodies/{id}")
    public ResponseEntity<Goodies> getGoodieById(@PathVariable Long id) {
        Goodies goodies = goodiesRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Goodie not exist with id :" + id));
        return ResponseEntity.ok(goodies);
    }

    @GetMapping("/goodies/kategorie/{kategorie}")
    public List<Goodies> getGoodieByKategorie(@PathVariable String kategorie){
        try {
           List<Goodies> goodies = goodiesRepository.getGoodiesByKategorie(kategorie);
            return goodies;
        } catch (BadRequestException e) {
            throw new BadRequestException("Versuchen Sie die Seite neu zu laden");
        }
    }
    @GetMapping("/goodies/kategorie/id/{kategorie}")
    public List<Long> getGoodieIdByKategorie(@PathVariable String kategorie){
        try {
            List<Long> goodiesId = goodiesRepository.getGoodiesIdByKategorie(kategorie);
            return goodiesId;
        } catch (BadRequestException e) {
            throw new BadRequestException("Versuchen Sie die Seite neu zu laden");
        }
    }

    @GetMapping("/goodies/name/{id}")
    public String getGoodieNameById(@PathVariable Long id){
        try{
            Goodies goodie = goodiesRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Goodie not exist with id :" + id));
            return goodie.getName();
        } catch (BadRequestException e) {
            throw new BadRequestException("Versuchen Sie die Seite neu zu laden");
        }
    }

    @GetMapping("/goodies/comment/{id}")
    public String getGoodieCommentById(@PathVariable Long id){
        try{
            Goodies goodie = goodiesRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Goodie not exist with id :" + id));
            return goodie.getComment();
        } catch (BadRequestException e) {
            throw new BadRequestException("Versuchen Sie die Seite neu zu laden");
        }
    }

    // create goodie rest api
    @PostMapping("/goodies")
    public Goodies createGoodie(@RequestBody Goodies goodies) {
        return goodiesRepository.save(goodies);
    }

    @PutMapping("/goodies/{id}")
    public ResponseEntity<Goodies> updateGoodieById(@RequestBody Goodies goodies, @PathVariable Long id){
        Goodies goodies1 = goodiesRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Goodie not exist with id :" + id));
        goodies1.setName(goodies.getName());
        goodies1.setComment(goodies.getComment());
        final Goodies updatedGoodie = goodiesRepository.save(goodies1);
        return ResponseEntity.ok(updatedGoodie);
    }

    @GetMapping("/goodiesPicture/{id}")
    public String getBaseStringById(@PathVariable Long id){
        Optional<Goodies> goodies = goodiesRepository.findById(id);
        return goodies.get().getBaseString();
    }

    @PutMapping("/goodiesPicture/{id}")
    public ResponseEntity<Goodies> updateGoodieBaseStringById(@RequestBody Goodies goodieBaseString, @PathVariable Long id){
        Goodies goodie = goodiesRepository.findById(id)
               .orElseThrow(() -> new ResourceNotFoundException("Goodie not exist with id :" + id));
        goodie.setBaseString(goodieBaseString.getBaseString());
        final Goodies updatedGoodie = goodiesRepository.save(goodie);
        return ResponseEntity.ok(updatedGoodie);
    }
}
