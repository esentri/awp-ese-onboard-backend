package com.example.backend.controller;

import com.example.backend.exception.ResourceNotFoundException;
import com.example.backend.model.Bilder;
import com.example.backend.model.Goodies;
import com.example.backend.repository.BilderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class BilderController {

    @Autowired
    private BilderRepository bilderRepository;

    // get all attachments
    @GetMapping("/bilder")
    public List<Bilder> getAllBilder(){
        return bilderRepository.findAll();
    }

    // get attachment by id rest api
    @GetMapping("/bilder/{id}")
    public ResponseEntity<Bilder> getBilderById(@PathVariable Long id) {
        Bilder bilder = bilderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Attachment not exist with id :" + id));
        return ResponseEntity.ok(bilder);
    }

    // create attachment rest api
    @PostMapping("/bilder")
    public Bilder createBilder(@RequestBody Bilder bilder) {
        return bilderRepository.save(bilder);
    }



}
