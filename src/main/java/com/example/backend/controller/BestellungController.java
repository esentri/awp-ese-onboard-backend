package com.example.backend.controller;

import com.example.backend.exception.BadRequestException;
import com.example.backend.exception.ResourceNotFoundException;
import com.example.backend.model.Bestellung;
import com.example.backend.repository.BestellungRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class BestellungController {

    @Autowired
    private BestellungRepository bestellungRepository;

    // get all bestellungen
    @GetMapping("/bestellung")
    public List<Bestellung> getAllBestellungen(){
        return bestellungRepository.findAll();
    }

    // get bestellung by id rest api
    @GetMapping("/bestellung/{id}")
    public ResponseEntity<Bestellung> getBestellungById(@PathVariable Long id) {
        try {
            Bestellung bestellung = bestellungRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Bestellung not exist with id :" + id));
            return ResponseEntity.ok(bestellung);
        } catch (BadRequestException e){
            throw new BadRequestException("Seite neu laden");
        }
    }

    //get all goodiesIds from bestellung_goodies by userId from bestellung
    @GetMapping("/bestellung/produkte/{id}")
    public List <Long> getGoodiesByUser(@PathVariable Long id) {
        try {
            List<Long> getGoodies = bestellungRepository.findGoodiesIdByUser(id);
            return getGoodies;
        } catch (ResourceNotFoundException e){
            throw new ResourceNotFoundException("User exisitert nicht mit id: " + id);
        } catch (BadRequestException e){
            throw new BadRequestException("Seite neu laden");
        }
    }

    // create bestellung rest api
    @PostMapping("/bestellung")
    public Bestellung createBestellung(@RequestBody Bestellung bestellung) {
        return bestellungRepository.save(bestellung);
    }

    @DeleteMapping("/bestellung/{id}")
    public void deleteBestellung(@PathVariable Long id){
        Long bgId = bestellungRepository.getIdByBestellungsGoodieId(id);
        bestellungRepository.deleteById(bgId);
    }

}
