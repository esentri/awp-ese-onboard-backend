package com.example.backend.controller;

import com.example.backend.exception.BadRequestException;
import com.example.backend.exception.ResourceNotFoundException;
import com.example.backend.model.Wunschbox;
import com.example.backend.repository.WunschboxRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class WunschboxController {

    @Autowired
    private WunschboxRepository wunschboxRepository;

    @GetMapping("/wunschbox")
    public List<Wunschbox> getAllWunschboxen(){return wunschboxRepository.findAll(); }

    @GetMapping("/wunschbox/{id}")
    public ResponseEntity<Wunschbox> getWunschboxById(@PathVariable Long id) {
        try {
            Wunschbox wunschbox = wunschboxRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Wunschbox not exist with id :" + id));
            return ResponseEntity.ok(wunschbox);
        } catch (BadRequestException e){
            throw new BadRequestException("Seite neu laden");
        }
    }

    @PostMapping("/wunschbox")
    public Wunschbox createWunschbox(@RequestBody Wunschbox wunschbox) {
        try {
            return wunschboxRepository.save(wunschbox);
        } catch (ResourceNotFoundException e){
            throw new ResourceNotFoundException("Wunschbox wurde nicht gefunden");
        }
    }

    @GetMapping("/wunschbox/benutzer/{id}")
    public Wunschbox getWunschboxByBenutzer(@PathVariable Long id){
        try{
            Wunschbox wunschbox = wunschboxRepository.findWunschboxByBenutzerId(id);
            return wunschbox;
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("Wunschbox wurde nicht gefunden");
        }
    }

    @GetMapping("/wunschbox/text/{id}")
    public String getWunschboxTextByBenutzerId(@PathVariable Long id){
        try{
            Wunschbox wunschbox = wunschboxRepository.findWunschboxByBenutzerId(id);
            return wunschbox.getText();
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("Wunschbox wurde nicht gefunden");
        }
    }

    @PutMapping("/wunschbox/text/{id}")
    public Wunschbox updateWunschboxByBenutzerId(@RequestBody Wunschbox wunschbox, @PathVariable Long id){
        try{
            Wunschbox w = wunschboxRepository.findWunschboxByBenutzerId(id);
            w.setText(wunschbox.getText());
            final Wunschbox updatedWunschbox = wunschboxRepository.save(w);
            return updatedWunschbox;
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("Wunschbox wurde nicht gefunden");
        }
    }
}
