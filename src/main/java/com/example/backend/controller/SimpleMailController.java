package com.example.backend.controller;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.example.backend.model.Goodies;
import com.example.backend.repository.BestellungRepository;
import com.example.backend.repository.GoodiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class SimpleMailController {
    @Autowired
    private JavaMailSender sender;
    @Autowired
    private BestellungController bestellungController;
    @Autowired
    private GoodiesController goodiesController;
    @Autowired
    private WunschboxController wunschboxController;

    @Autowired
    private BenutzerController benutzerController;

    @RequestMapping(value = "/sendMail", method = RequestMethod.GET)
    public String sendMail() {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        //alle GoodiesIds für den angemeldeten Benutzer holen
        Long benutzerId = 1L;
        List<Long> goodiesIds = bestellungController.getGoodiesByUser(benutzerId);

        //Für jede Id den dazugehörigen Goodie in List speichern
        ArrayList<ResponseEntity<Goodies>> alleGoodies = new ArrayList<>();
        for (Long l: goodiesIds) {
            ResponseEntity<Goodies> goodie = goodiesController.getGoodieById(l);
            alleGoodies.add(goodie);
        }

        String größe = benutzerController.getSizeById(benutzerId);
        String wunschText = wunschboxController.getWunschboxTextByBenutzerId(benutzerId);

        //Email Inhalt erstellen
        StringBuilder str = new StringBuilder();
        str.append("Hallo,\n");
        str.append("es ist eine neue Bestellung von folgenden Nutzer eingegangen: Max Mustermann \n");
        for (ResponseEntity<Goodies> g: alleGoodies) {
            str.append(g.getBody().getKategorie() + ": ");
            str.append(g.getBody().getName() + ",");
            str.append("\n");
        }
        str.append("Kleidergröße: " + größe);
        str.append("\nWeitere Wünsche: " + wunschText);
        str.append("\nDies ist eine automatisch generierte Email.");

        try {
            helper.setTo("demo@gmail.com");
            helper.setText(str.toString());
            helper.setSubject("Mail From Spring Boot");
        } catch (MessagingException e) {
            e.printStackTrace();
            return "Error while sending mail ..";
        }
        sender.send(message);
        return "Mail Sent Success!";
    }
}
