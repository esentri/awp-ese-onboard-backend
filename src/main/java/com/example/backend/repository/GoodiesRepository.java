package com.example.backend.repository;

import com.example.backend.model.Goodies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodiesRepository extends JpaRepository<Goodies, Long> {

    @Query("select g from Goodies g where g.kategorie = :kategorie")
    public List<Goodies> getGoodiesByKategorie (@Param("kategorie") String kategorie);

    @Query("select g.id from Goodies g where g.kategorie = :kategorie")
    public List<Long> getGoodiesIdByKategorie (@Param("kategorie") String kategorie);

    @Query("select g.baseString from Goodies g where g.id = :id")
    public String getBaseString (@Param("id") Long id);
}