package com.example.backend.repository;

import com.example.backend.model.ErsterTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ErsterTagRepository extends JpaRepository<ErsterTag, Long> {

    @Query("select eT.dashboardText from ErsterTag eT where eT.id = :ersterTagId")
    public String findErsterTagTextById(@Param("ersterTagId") Long ersterTagId);
}
