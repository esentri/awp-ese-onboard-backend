package com.example.backend.repository;

import com.example.backend.model.Bestellung;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BestellungRepository extends JpaRepository<Bestellung, Long> {
    @Query("select bg.goodiesId from BestellungGoodies bg join Bestellung b on bg.id = b.bestellungGoodiesId where b.benutzerId = :benutzerId")
    public List<Long> findGoodiesIdByUser(@Param("benutzerId") Long benutzerId);

    @Query("select b from Bestellung b where b.benutzerId = :benutzerId")
    public Bestellung findBestellungByUser(@Param("benutzerId") Long benutzerId);

    @Query("Select id from Bestellung where bestellungGoodiesId = :bestellungGoodiesId")
    public Long getIdByBestellungsGoodieId(@Param("bestellungGoodiesId") Long bestellungGoodiesId);
}
