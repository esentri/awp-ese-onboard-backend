package com.example.backend.repository;

import com.example.backend.model.Wunschbox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WunschboxRepository extends JpaRepository<Wunschbox, Long> {

    @Query("select w from Wunschbox w where w.benutzerId = :benutzerId")
    public Wunschbox findWunschboxByBenutzerId(@Param("benutzerId") Long benutzerId);
}
