package com.example.backend.repository;

import com.example.backend.model.Bestellung;
import com.example.backend.model.BestellungGoodies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BestellungGoodiesRepository extends JpaRepository<BestellungGoodies, Long> {

    @Query("select bg.id from BestellungGoodies bg join Bestellung b on b.bestellungGoodiesId = bg.id where b.benutzerId = :benutzerId")
    public List<Long> getBestellungGoodiesIdByBenutzer(@Param("benutzerId") Long benutzerId);

    @Query("select bg.id from BestellungGoodies bg join Goodies g on g.id = bg.goodiesId where g.kategorie =:kategorie")
    public List<Long> getBestellungGoodiesIdByKategorie(@Param("kategorie") String kategorie);

    @Query("select max(id) from BestellungGoodies")
    public Long getLastInsertedId();
}
