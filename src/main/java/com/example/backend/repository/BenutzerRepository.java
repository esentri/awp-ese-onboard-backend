package com.example.backend.repository;

import com.example.backend.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BenutzerRepository extends JpaRepository<Users, Long> {
    @Query("select u from Users u where u.kreisId = :kreisId")
    public List<Users> findUserByKreis(@Param("kreisId") Long kreisId);

    @Query("select u from Users u where u.id = :id")
    public Users findUserById(@Param("id") Long id);

    @Query("select u from Users u where u.name = :name")
    public Users findUserByName(@Param("name") String name);

    @Query("select bestellt from Users where id = :id")
    public int findBestelltById(@Param("id") Long id);

}
