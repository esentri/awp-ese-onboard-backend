package com.example.backend.repository;

import com.example.backend.model.Kreis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KreisRepository extends JpaRepository<Kreis, Long> {
}
