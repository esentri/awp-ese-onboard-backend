package com.example.backend.repository;

import com.example.backend.model.Kalender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KalenderRepository extends JpaRepository<Kalender, Long> {
}
